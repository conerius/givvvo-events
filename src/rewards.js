import React, { useState, useEffect } from 'react';

import './App.css';

import logo from './assets/new/logoPresents.svg';
import givvoX from './assets/new/givvoXtheOdds.svg';

import logo1 from './assets/new/moreLogo.svg';
import logo2 from './assets/new/peanuts.svg';
import logo3 from './assets/new/funRadio.svg';
import logo4 from './assets/new/fameMenagment.svg';
import logo5 from './assets/new/vitaVape.svg';
// import logo6 from './assets/new/royalCatering.svg';
import logo7 from './assets/new/vinarstvo.svg';

import logo8 from './assets/new/predpredaj.svg';
import logo9 from './assets/new/zoznam.svg';

import icon1 from './assets/new/Location.svg';
import icon2 from './assets/new/clock.svg';
import icon3 from './assets/new/Ticket.svg';
import girl from './assets/new/girl.png';
import girlPhone from './assets/new/girlPhone.png';
import ig1 from './assets/new/ig1.svg';
import ig2 from './assets/new/ig2.svg';
import bbb from './assets/new/bbb.png';
import airpods from './assets/new/airpods.png';
import iphone from './assets/new/iphone.png';
import rolex from './assets/new/rolex.png';
import macbook from './assets/new/macbook.png';
import whatch from './assets/new/whatch.png';
import backModal from './assets/new/backModal.png';

import win from './assets/new/win.svg';
import winner from './assets/new/winner.png';

import info from './assets/new/info.svg';

import Countdown from 'react-countdown';
import axios from 'axios';

import ReactPixel from 'react-facebook-pixel';

import Modal from 'react-modal';

const Rewards = () => {
  const [modal, setModal] = useState(false);

  // const renderer = ({ days, hours, minutes, seconds, completed }) => {
  //   if (completed) {
  //     // Render a completed state
  //     return 'Time is up!';
  //   } else {
  //     // Render a countdown
  //     return (
  //       <span>
  //         {days} days {hours} hours {minutes} minutes
  //       </span>
  //     );
  //   }
  // };

  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   try {
  //     const data = await axios.post(
  //       'https://us-central1-xpressrx-delivery.cloudfunctions.net/app/mail/send',
  //       { email }
  //     );
  //     console.log(data);
  //     setSuccess('Event ticket inquiry sent');
  //     setTimeout(() => {
  //       setSuccess('');
  //     }, 3000);
  //   } catch (e) {
  //     console.log(e.response);
  //     setSuccess('Error');
  //     setTimeout(() => {
  //       setSuccess('');
  //     }, 3000);
  //   }
  // };

  // useEffect(() => {
  //   const advancedMatching = { em: 'some@email.com' }; // optional, more info: https://developers.facebook.com/docs/facebook-pixel/advanced/advanced-matching
  //   const options = {
  //     autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
  //     debug: true, // enable logs
  //   };

  //   ReactPixel.init('911093319502342', advancedMatching, options);
  //   ReactPixel.pageView();
  // }, []);

  return (
    <>
      <div className='wrapper'>
        <div>
          <div className='background'>
            <div className='heroRewards'>
              <div className='logosHeroRewards'>
                <img src={logo} alt='logo' className='logo' />
                <img src={givvoX} alt='logo' className='logo' />
              </div>
              <div className='heroBigRewards'>
                <div className='gradientText'>“THIS FASHION EVENT</div>
                <span>CHANGES EVERYTHING”</span>
              </div>
              <div className='rewards'>
                <div className='reward'>
                  <div className='gradientBorderWrapReward'>
                    <div
                      className='gradientBorderReward'
                      style={{ padding: 10, position: 'relative ' }}
                    >
                      <img src={bbb} alt='bbb' style={{ width: 270 }} />
                      <div className='unutar'>
                        <p>APPLE AIRPODS PRO</p>
                        <img src={airpods} alt='bbb' style={{ height: 120 }} />
                        <button
                          className='btnReward'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                          onClick={() => setModal(true)}
                        >
                          Select a Winner
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='reward'>
                  <div className='gradientBorderWrapReward'>
                    <div
                      className='gradientBorderReward'
                      style={{ padding: 10, position: 'relative ' }}
                    >
                      <img src={bbb} alt='bbb' style={{ width: 270 }} />
                      <div className='unutar'>
                        <p>APPLE WATCH SERIES 6</p>
                        <img src={whatch} alt='bbb' style={{ height: 120 }} />
                        <button
                          className='btnReward'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                        >
                          Select a Winner
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='reward'>
                  <div className='gradientBorderWrapReward'>
                    <div
                      className='gradientBorderReward'
                      style={{ padding: 10, position: 'relative ' }}
                    >
                      <img src={bbb} alt='bbb' style={{ width: 270 }} />
                      <div className='unutar'>
                        <p>APPLE IPHONE 12 PRO</p>
                        <img src={iphone} alt='bbb' style={{ height: 120 }} />
                        <button
                          className='btnReward'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                        >
                          Select a Winner
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='reward'>
                  <div className='gradientBorderWrapReward'>
                    <div
                      className='gradientBorderReward'
                      style={{ padding: 10, position: 'relative ' }}
                    >
                      <img src={bbb} alt='bbb' style={{ width: 270 }} />
                      <div className='unutar'>
                        <p>APPLE MACBOOK AIR M1</p>
                        <img src={macbook} alt='bbb' style={{ height: 120 }} />
                        <button
                          className='btnReward'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                        >
                          Select a Winner
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='reward'>
                  <div className='gradientBorderWrapReward'>
                    <div
                      className='gradientBorderReward'
                      style={{ padding: 10, position: 'relative ' }}
                    >
                      <img src={bbb} alt='bbb' style={{ width: 270 }} />
                      <div className='unutar'>
                        <p>ROLEX DATEJUST WIMBLEDON</p>
                        <img src={rolex} alt='bbb' style={{ height: 120 }} />
                        <button
                          className='btnReward'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                        >
                          Select a Winner
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className='hr' />
              <img
                src={win}
                alt='bbb'
                style={{
                  position: 'fixed',
                  top: 380,
                  right: '-160px',
                  height: '350px',
                }}
              />
            </div>
          </div>
        </div>

        <Modal
          isOpen={modal}
          onRequestClose={() => setModal(false)}
          contentLabel='My dialog'
          className='mymodal'
          overlayClassName='myoverlay'
          closeTimeoutMS={500}
          ariaHideApp={false}
        >
          <div className='gradientBorderWrapReward'>
            <div
              className='gradientBorderReward'
              style={{ padding: 20, position: 'relative ' }}
            >
              <img src={backModal} alt='back' style={{ width: 1000 }} />
              <div className='winner'>
                <div className='winnerInner'>
                  <img src={winner} alt='back' />
                  <div className='winCode'> 2XD - 54A - 7DD </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>

        {/* {success && (
          <div
            style={{
              position: 'fixed',
              left: 'auto',
              right: 'auto',
              top: '100px',
              width: 200,
              background: '#1f232b',
              height: 100,
              borderRadius: 13,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              fontSize: 25,
              textAlign: 'center',
            }}
          >
            <div className='gradientText'> {success}</div>
          </div>
        )} */}
      </div>
    </>
  );
};

export default Rewards;
