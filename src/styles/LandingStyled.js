// import styled from 'styled-components';

// const LandingWrapper = styled.div`
//   max-width: 1629px;
//   width: 100%;
//   height: 100vh;
//   margin: 0 auto;
//   //   background: #eaeaea;
//   position: relative;
//   @media (max-width: 1024px) {
//     display: none;
//   }
// `;
// const Landing = styled.div`
//   display: flex;
//   justify-content: space-between;
// `;

// const Logo = styled.div`
//   margin: 0 0 24px 0;
//   /* position: fixed;
//   top: 0;
//   left: 0;
//   right: 0; */
//   img {
//     width: 253px;
//   }
// `;

// const Title = styled.div`
//   font-style: normal;
//   font-weight: bold;
//   font-size: 80px;
//   line-height: 95px;
//   color: #4e4e4e;
//   span {
//     color: #d477ad;
//   }
// `;

// const SubTitle = styled.div`
//   font-style: normal;
//   font-weight: bold;
//   font-size: 60px;
//   line-height: 71px;
//   color: #ffffff;
//   span {
//     color: #4e4e4e;
//   }
// `;

// const InputContainer = styled.div`
//   height: 74px;
//   width: 403px;
//   background: #ffffff;
//   display: flex;
//   justify-content: center;
//   padding: 0 20px;
//   flex-direction: column;
//   input {
//     width: 100%;
//     font-weight: 500;
//     font-size: 20px;
//     line-height: 24px;
//     color: #4e4e4e;
//     border: none;
//     :focus {
//       outline: none;
//     }
//   }
// `;

// const Button = styled.button`
//   background: none;
//   border: none;
//   padding: 0;
//   margin-left: 20px;
//   cursor: pointer;
// `;
// const Sect1 = styled.div`
//   height: 863px;
//   display: flex;
//   align-items: flex-end;
//   width: 500px;
//   div {
//     width: 100%;
//     img {
//       width: 100%;
//     }
//   }
// `;
// const Sect2 = styled.div`
//   display: flex;
//   flex-direction: column;
//   justify-content: space-between;
// `;
// const Sect3 = styled.div``;

// const PhoneLanding = styled.div`
//   width: 100%;
//   display: none;
//   @media (max-width: 1024px) {
//     display: block;
//   }
// `;

// const PhoneTitle = styled(Title)`
//   font-size: 38px;
//   position: fixed;
//   text-align: center;
//   top: 80px;
//   left: 0;
//   right: 0;
// `;

// const PhoneSubTitle = styled(SubTitle)`
//   font-size: 35px;
//   text-align: center;
//   position: fixed;
//   bottom: 150px;
//   left: 0;
//   right: 0;
// `;

// const PhoneForm = styled.div`
//   width: 100%;
//   padding: 0 20px;
//   position: fixed;
//   bottom: 24px;
//   left: 0;
//   right: 0;
//   display: flex;
// `;

// const PhoneInputContainer = styled(InputContainer)`
//   width: calc(100% - 72px);
// `;

// const PhoneButton = styled(Button)`
//   img {
//     width: 52px;
//   }
// `;

// const PhoneLogo = styled(Logo)`
//   width: 100%;
//   display: flex;
//   justify-content: center;
//   z-index: 10000;
// `;

// const PhoneSect1 = styled.div`
//   margin-top: 120px;
//   width: 100%;
//   display: flex;
//   justify-content: space-between;
//   align-items: flex-end;
//   div {
//     width: 48%;
//     img {
//       width: 100%;
//       margin-bottom: 20px;
//     }
//   }
// `;

// const PhoneSect2 = styled.div`
//   width: 100%;
//   display: flex;
//   justify-content: space-between;
//   /* align-items: flex-end; */
//   div {
//     width: 48%;
//     img {
//       width: 100%;
//     }
//   }
// `;

// export {
//   LandingWrapper,
//   Title,
//   SubTitle,
//   Logo,
//   InputContainer,
//   Button,
//   Landing,
//   Sect1,
//   Sect2,
//   Sect3,
//   PhoneLanding,
//   PhoneTitle,
//   PhoneSubTitle,
//   PhoneInputContainer,
//   PhoneButton,
//   PhoneForm,
//   PhoneLogo,
//   PhoneSect1,
//   PhoneSect2,
// };
