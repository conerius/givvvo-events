import React, { useState, useEffect } from 'react';

import './App.css';

import logo from './assets/new/logoPresents.svg';
import givvoX from './assets/new/givvoXtheOdds.svg';

import logo1 from './assets/new/moreLogo.svg';
import logo2 from './assets/new/peanuts.svg';
import logo3 from './assets/new/funRadio.svg';
import logo4 from './assets/new/fameMenagment.svg';
import logo5 from './assets/new/vitaVape.svg';
// import logo6 from './assets/new/royalCatering.svg';
import logo7 from './assets/new/vinarstvo.svg';

import logo8 from './assets/new/predpredaj.svg';
import logo9 from './assets/new/zoznam.svg';

import icon1 from './assets/new/Location.svg';
import icon2 from './assets/new/clock.svg';
import icon3 from './assets/new/Ticket.svg';
import girl from './assets/new/girl.png';
import girlPhone from './assets/new/girlPhone.png';
import ig1 from './assets/new/ig1.svg';
import ig2 from './assets/new/ig2.svg';
import ig3 from './assets/new/ig3.svg';

import info from './assets/new/info.svg';

import Countdown from 'react-countdown';
import axios from 'axios';

import ReactPixel from 'react-facebook-pixel';
import { Link } from 'react-router-dom';

const Home = () => {
  const [email, setEmail] = useState('');
  const [success, setSuccess] = useState('');

  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return 'Time is up!';
    } else {
      // Render a countdown
      return (
        <span>
          {days} days {hours} hours {minutes} minutes
        </span>
      );
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const data = await axios.post(
        'https://us-central1-xpressrx-delivery.cloudfunctions.net/app/mail/send',
        { email }
      );
      console.log(data);
      setSuccess('Event ticket inquiry sent');
      setTimeout(() => {
        setSuccess('');
      }, 3000);
    } catch (e) {
      console.log(e.response);
      setSuccess('Error');
      setTimeout(() => {
        setSuccess('');
      }, 3000);
    }
  };

  useEffect(() => {
    const advancedMatching = { em: 'some@email.com' }; // optional, more info: https://developers.facebook.com/docs/facebook-pixel/advanced/advanced-matching
    const options = {
      autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
      debug: true, // enable logs
    };

    ReactPixel.init('911093319502342', advancedMatching, options);
    ReactPixel.pageView();
  }, []);

  return (
    <>
      <div className='wrapper'>
        <div>
          <div className='background'>
            <div className='hero'>
              <div className='logosHero'>
                <img src={logo} alt='logo' className='logo' />
                <img src={givvoX} alt='logo' className='logo' />
              </div>
              <div className='heroBig'>
                <div className='gradientText'>“THIS FASHION EVENT</div>
                <span>CHANGES EVERYTHING”</span>
              </div>

              <div className='heroSmall'>
                THE REVOUTIONARY EXPERIENCE <br />
                STARTS ON <span className='gradientText'>OCTOBER 1st</span>
                <br />
                FOR THE FIRST TIME IN HISTORY
              </div>

              <div className='heroAttendanceWrap'>
                <div className='heroAttendance'>
                  <div className='gradientBorderWrap'>
                    <div
                      className='gradientBorder'
                      style={{
                        padding: '20px 20px',
                        flexDirection: 'column',
                        position: 'relative',
                        alignItems: 'flex-start',
                      }}
                    >
                      <h3>Attendance requirements</h3>
                      <h4>Vaccinated, negative test, mask</h4>
                      <img
                        style={{ position: 'absolute', top: 15, right: 15 }}
                        src={info}
                        alt='info'
                        className='info'
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className='gradientBorderWrap send'>
                <div className='gradientBorder' style={{ padding: 20 }}>
                  <div className='send23' style={{}}>
                    <div className='label'>
                      <img src={icon1} alt='ic' /> LOCATION
                    </div>
                    <p>Incheba, Bratislava, Slovakia</p>
                  </div>
                  <div className='send23' style={{}}>
                    <div className='label'>
                      <img src={icon2} alt='ic' />
                      WHEN
                    </div>
                    <p>October 1st, 2021 - 8 PM</p>
                  </div>
                  <div className='send48'>
                    <div className='label'>
                      <img src={icon3} alt='ic' />
                      GET TICKETS
                    </div>
                    <form onSubmit={(e) => handleSubmit(e)}>
                      <div className='inputBtn'>
                        <div
                          className='inputWrapper'
                          style={{ position: 'relative' }}
                        >
                          <input
                            placeholder='Enter your email'
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            type='email'
                          />
                        </div>
                        <button
                          className='btn'
                          type='submit'
                          style={{ cursor: 'pointer' }}
                        >
                          Send inquiry
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='background2'>
            <div className='main'>
              <div className='logosMain'>
                <img src={logo1} alt='logo' className='logo' />{' '}
                <img src={logo2} alt='logo' className='logo' />{' '}
                <img src={logo3} alt='logo' className='logo' />{' '}
                <img src={logo4} alt='logo' className='logo' />{' '}
                <img src={logo5} alt='logo' className='logo' />{' '}
                {/* <img src={logo6} alt='logo' className='logo' />{' '} */}
                <img src={logo7} alt='logo' className='logo' />
                <img src={logo8} alt='logo' className='logo' />
                <img src={logo9} alt='logo' className='logo' />
              </div>
              {/* <div className='predpredaj'>
              </div> */}

              <div className='untilShowtime'>Time left until showtime:</div>
              <div className='daysHours gradientText'>
                <Countdown date={1633111200000} renderer={renderer} />
              </div>
              <div className='gradientBorderWrap'>
                <div className='gradientBorder'>
                  <div className='width60'>
                    <h4>
                      Want to play a part in the fashion event of the year{' '}
                      <Link
                        to='/rewards'
                        style={{ color: '#fff', cursor: 'default' }}
                      >
                        ?
                      </Link>{' '}
                      <br />
                      Join us as SPONSORS / INFLUENCERS / PRESS
                    </h4>
                    <h5>
                      Contact us at
                      <span className='gradientText'>
                        <b> events@givvo.com</b>
                      </span>
                    </h5>
                  </div>
                  <div className='girl'>
                    <img src={girl} alt='girl' />
                  </div>
                  <div className='girlPhone'>
                    <img src={girlPhone} alt='girl' />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='background2'>
            <div className='hr' />
          </div>

          <div className='background2'>
            <div className='footer'>
              <div
                className='footerSection'
                style={{
                  fontWeight: '700',
                }}
              >
                Follow us at{' '}
              </div>
              <a href='https://www.instagram.com/givvo.official/?hl=en'>
                <div className='footerSection'>
                  GIVVO <img src={ig1} alt='instagram' />
                </div>
              </a>
              <a href='https://www.instagram.com/wear.theodds/?hl=en'>
                <div className='footerSection'>
                  The ODDS <img src={ig2} alt='instagram' />
                </div>
              </a>
              <a href='https://www.instagram.com/more_foundation_charity/'>
                <div className='footerSection'>
                  More Foundation <img src={ig3} alt='instagram' />
                </div>
              </a>
            </div>
          </div>
        </div>

        {success && (
          <div
            style={{
              position: 'fixed',
              left: 'auto',
              right: 'auto',
              top: '100px',
              width: 200,
              background: '#1f232b',
              height: 100,
              borderRadius: 13,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              fontSize: 25,
              textAlign: 'center',
            }}
          >
            <div className='gradientText'> {success}</div>
          </div>
        )}
      </div>
    </>
  );
};

export default Home;
