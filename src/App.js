import React, { useState, useEffect } from 'react';

import './App.css';

// import logo from './assets/new/logoPresents.svg';
// import givvoX from './assets/new/givvoXtheOdds.svg';

// import logo1 from './assets/new/moreLogo.svg';
// import logo2 from './assets/new/peanuts.svg';
// import logo3 from './assets/new/funRadio.svg';
// import logo4 from './assets/new/fameMenagment.svg';
// import logo5 from './assets/new/vitaVape.svg';
// // import logo6 from './assets/new/royalCatering.svg';
// import logo7 from './assets/new/vinarstvo.svg';

// import logo8 from './assets/new/predpredaj.svg';
// import logo9 from './assets/new/zoznam.svg';

// import icon1 from './assets/new/Location.svg';
// import icon2 from './assets/new/clock.svg';
// import icon3 from './assets/new/Ticket.svg';
// import girl from './assets/new/girl.png';
// import girlPhone from './assets/new/girlPhone.png';
// import ig1 from './assets/new/ig1.svg';
// import ig2 from './assets/new/ig2.svg';
// import ig3 from './assets/new/ig3.svg';

// import info from './assets/new/info.svg';

// import Countdown from 'react-countdown';
// import axios from 'axios';

// import ReactPixel from 'react-facebook-pixel';

import Home from './home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Rewards from './rewards';

const App = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/rewards' component={Rewards} />
        </Switch>
      </Router>
    </>
  );
};

export default App;
